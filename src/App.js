import React, { Component } from 'react';
import _ from 'lodash'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      taskId: props.taskId,
      status: 'Submitted',
      updates: []
    }
  }
  componentDidMount() {
    this.getUpdates();
  }
  getUpdates() {
    console.log('getting new data..');
    fetch('http://localhost:8080/struts-offline-actions/rest/task/status?taskId=' + this.state.taskId)
      .then(function (response) {
        return response.json();
      })
      .then((myJson) => {
        this.setState({
          updates: _.unionBy(this.state.updates, myJson, "update")
        }, () => {
          if (myJson.length > 0) {
            if (myJson.length >= 2)
              this.setState({ status: 'Processing' })
            setTimeout(() => { this.getUpdates() }, 5000);
          }
          else {
            console.log('no more data required');
            this.setState({ status: 'Completed' })
          }
        })
      })
      .catch((err) => {
        // response 
        console.log(err);
      })
  }
  getRelativeTime = (when) => {
    const msPerMinute = 60 * 1000;
    const msPerHour = msPerMinute * 60;
    const month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let elapsed = Date.now() - when;
    const date = new Date(when);

    if (elapsed < msPerMinute) {
      return 'few seconds ago';
    }

    else if (elapsed < msPerHour) {
      return Math.round(elapsed / msPerMinute) + ' minutes ago';
    }

    else {
      return `'${month[date.getMonth()]} ${date.getDate()} , ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}'`;
    }
  }
  render() {
    const { updates, taskId, status } = this.state;

    return (
      <div style={{ padding: 10 }}>
        <div style={{ fontWeight: 'bold', fontSize: 16 }}>TaskId: {taskId}</div>
        <div style={{ fontSize: 16 }}>Status: {status}</div>

        <h4>Updates</h4>
        {updates.map((item, index) =>
          <div key={index} style={{ paddingBottom: 10 }}>
            <div style={{ fontSize: 16 }}>{item.update}</div>
            <div style={{ fontSize: 12, opacity: 0.5 }}>{this.getRelativeTime(item.when)}</div>
          </div>
        )}
      </div>
    );
  }
}

export default App;
